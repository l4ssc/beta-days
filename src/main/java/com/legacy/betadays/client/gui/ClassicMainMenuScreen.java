package com.legacy.betadays.client.gui;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.legacy.betadays.BetaDays;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.GameSettings;
import net.minecraft.client.gui.AccessibilityScreen;
import net.minecraft.client.gui.screen.LanguageScreen;
import net.minecraft.client.gui.screen.MainMenuScreen;
import net.minecraft.client.gui.screen.MultiplayerScreen;
import net.minecraft.client.gui.screen.OptionsScreen;
import net.minecraft.client.gui.screen.PackScreen;
import net.minecraft.client.gui.screen.WorldSelectionScreen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.ImageButton;
import net.minecraft.resources.ResourcePackInfo;
import net.minecraft.resources.ResourcePackList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SharedConstants;
import net.minecraft.util.Util;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.client.gui.screen.ModListScreen;

public class ClassicMainMenuScreen extends MainMenuScreen
{
	private static final ResourceLocation MINECRAFT_TITLE_TEXTURES = new ResourceLocation("textures/gui/title/minecraft.png");
	private static final ResourceLocation ACCESSIBILITY_TEXTURES = new ResourceLocation("textures/gui/accessibility.png");
	private static final ResourceLocation FORGE_BUTTON = BetaDays.locate("textures/gui/forge.png");
	private final boolean showFadeInAnimation;
	private String splashText;
	private int widthCopyright;
	private int widthCopyrightRest;
	private long firstRenderTime;

	public ClassicMainMenuScreen()
	{
		this(false);
	}

	public ClassicMainMenuScreen(boolean fadeIn)
	{
		this.showFadeInAnimation = fadeIn;
	}

	@Override
	protected void init()
	{
		if (this.splashText == null)
		{
			this.splashText = this.minecraft.getSplashes().getSplashText();
		}

		this.widthCopyright = this.font.getStringWidth("Copyright Mojang AB. Do not distribute.");
		this.widthCopyrightRest = this.width - this.widthCopyright - 2;
		int j = this.height / 4 + 48;
		this.addSingleplayerMultiplayerButtons(j, 24);

		this.addButton(new ImageButton(this.width - 25, 3 * 2, 20, 20, 0, 0, 20, FORGE_BUTTON, 32, 64, (onPress) ->
		{
			this.minecraft.displayGuiScreen(new ModListScreen(this));
		}));

		this.addButton(new ImageButton(this.width - 25, 3 * 18, 20, 20, 0, 106, 20, Button.WIDGETS_LOCATION, 256, 256, (onPress) ->
		{
			this.minecraft.displayGuiScreen(new LanguageScreen(this, this.minecraft.gameSettings, this.minecraft.getLanguageManager()));
		}, new TranslationTextComponent("narrator.button.language")));
		this.addButton(new Button(this.width / 2 - 100, j + (24 * 2), 200, 20, new TranslationTextComponent("menu.options"), (onPress) ->
		{
			this.minecraft.displayGuiScreen(new OptionsScreen(this, this.minecraft.gameSettings));
		}));
		this.addButton(new Button(this.width / 2 - 100, j + (24 * 3), 200, 20, new TranslationTextComponent("beta.menu.modsTextures"), (onPress) ->
		{
			this.minecraft.displayGuiScreen(new PackScreen(this, this.minecraft.getResourcePackList(), this::func_241584_a_, this.minecraft.getFileResourcePacks(), new TranslationTextComponent("resourcePack.title")));
		}));
		this.addButton(new ImageButton(this.width - 25, 3 * 10, 20, 20, 0, 0, 20, ACCESSIBILITY_TEXTURES, 32, 64, (onPress) ->
		{
			this.minecraft.displayGuiScreen(new AccessibilityScreen(this, this.minecraft.gameSettings));
		}, new TranslationTextComponent("narrator.button.accessibility")));

		this.minecraft.setConnectedToRealms(false);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void render(MatrixStack stack, int mouseX, int mouseY, float partialTicks)
	{
		if (this.firstRenderTime == 0L && this.showFadeInAnimation)
		{
			this.firstRenderTime = Util.milliTime();
		}

		float f = this.showFadeInAnimation ? (float) (Util.milliTime() - this.firstRenderTime) / 1000.0F : 1.0F;
		fill(stack, 0, 0, this.width, this.height, -1);
		int j = this.width / 2 - 137;

		RenderSystem.enableBlend();
		RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
		RenderSystem.color4f(1.0F, 1.0F, 1.0F, this.showFadeInAnimation ? (float) MathHelper.ceil(MathHelper.clamp(f, 0.0F, 1.0F)) : 1.0F);
		blit(stack, 0, 0, this.width, this.height, 0.0F, 0.0F, 16, 128, 16, 128);
		float f1 = this.showFadeInAnimation ? MathHelper.clamp(f - 1.0F, 0.0F, 1.0F) : 1.0F;
		int l = MathHelper.ceil(f1 * 255.0F) << 24;

		if ((l & -67108864) != 0)
		{
			this.renderBackground(null, 0);
			this.minecraft.getTextureManager().bindTexture(MINECRAFT_TITLE_TEXTURES);
			RenderSystem.color4f(1.0F, 1.0F, 1.0F, f1);
			blit(stack, j + 0, 30, 0, 0, 155, 44);
			blit(stack, j + 155, 30, 0, 45, 155, 44);

			if (this.splashText != null)
			{
				RenderSystem.pushMatrix();
				RenderSystem.translatef((float) (this.width / 2 + 90), 70.0F, 0.0F);
				RenderSystem.rotatef(-20.0F, 0.0F, 0.0F, 1.0F);
				float f2 = 1.8F - MathHelper.abs(MathHelper.sin((float) (Util.milliTime() % 1000L) / 1000.0F * ((float) Math.PI * 2F)) * 0.1F);
				f2 = f2 * 100.0F / (float) (this.font.getStringWidth(this.splashText) + 32);
				RenderSystem.scalef(f2, f2, f2);
				drawCenteredString(stack, this.font, this.splashText, 0, -8, 16776960 | l);
				RenderSystem.popMatrix();
			}

			String s = "Minecraft " + SharedConstants.getVersion().getName();
			s = s + ("release".equalsIgnoreCase(this.minecraft.getVersionType()) ? "" : "/" + this.minecraft.getVersionType());

			String releaseType = this.minecraft.getVersionType();
			drawString(stack, this.font, TextFormatting.DARK_GRAY + "Minecraft " + releaseType.replace("r", "R") + " " + SharedConstants.getVersion().getName(), 2, 3 * 2, 16777215 | l);
			drawString(stack, this.font, "Copyright Mojang AB. Do not distribute.", this.widthCopyrightRest, this.height - 10, 16777215 | l);

			for (Widget widget : this.buttons)
			{
				widget.setAlpha(f1);
			}
		}

		for (int r = 0; r < this.buttons.size(); ++r)
		{
			this.buttons.get(r).render(stack, mouseX, mouseY, partialTicks);
		}

	}

	private void addSingleplayerMultiplayerButtons(int yIn, int rowHeightIn)
	{
		this.addButton(new Button(this.width / 2 - 100, yIn, 200, 20, new TranslationTextComponent("menu.singleplayer"), (p_213089_1_) ->
		{
			this.minecraft.displayGuiScreen(new WorldSelectionScreen(this));
		}));
		this.addButton(new Button(this.width / 2 - 100, yIn + rowHeightIn * 1, 200, 20, new TranslationTextComponent("menu.multiplayer"), (p_213086_1_) ->
		{
			this.minecraft.displayGuiScreen(new MultiplayerScreen(this));
		}));
	}

	private void func_241584_a_(ResourcePackList p_241584_1_)
	{
		List<String> list = ImmutableList.copyOf(this.minecraft.gameSettings.resourcePacks);
		this.minecraft.gameSettings.resourcePacks.clear();
		this.minecraft.gameSettings.incompatibleResourcePacks.clear();

		for (ResourcePackInfo resourcepackinfo : p_241584_1_.getEnabledPacks())
		{
			if (!resourcepackinfo.isOrderLocked())
			{
				this.minecraft.gameSettings.resourcePacks.add(resourcepackinfo.getName());
				if (!resourcepackinfo.getCompatibility().isCompatible())
				{
					this.minecraft.gameSettings.incompatibleResourcePacks.add(resourcepackinfo.getName());
				}
			}
		}

		this.minecraft.gameSettings.saveOptions();
		List<String> list1 = ImmutableList.copyOf(this.minecraft.gameSettings.resourcePacks);
		if (!list1.equals(list))
		{
			this.minecraft.reloadResources();
		}

	}
}
