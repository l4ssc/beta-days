package com.legacy.betadays.client;

import java.util.Random;

import com.legacy.betadays.BetaDaysConfig;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.ai.attributes.ModifiableAttributeInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effects;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SharedConstants;
import net.minecraft.util.Util;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.client.event.FOVUpdateEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BetaOverlayEvent
{

	private static final ResourceLocation ICONS = new ResourceLocation("textures/gui/icons.png");
	private static int left_height = 39;
	private static int right_height = 39;
	private Random rand = new Random();
	private int updateDelay, updateCounter, playerHealth, lastPlayerHealth;
	private long healthUpdateCounter, lastSystemTime;
	private float prevFOV = 0;

	@SubscribeEvent
	public void onOverlayEvent(RenderGameOverlayEvent event)
	{
		Minecraft mc = Minecraft.getInstance();
		boolean shouldDraw = (mc.playerController.shouldDrawHUD() && mc.getRenderViewEntity() instanceof PlayerEntity);

		if (BetaDaysConfig.oldHud)
		{

			if (BetaDaysConfig.disableExperienceDrop && event.getType() == ElementType.EXPERIENCE || event.getType() == ElementType.ARMOR || event.getType() == ElementType.HEALTH || event.getType() == ElementType.FOOD || event.getType() == ElementType.HEALTHMOUNT || event.getType() == ElementType.AIR)
			{
				event.setCanceled(true);
			}

			this.updateHealthTicks();

			if (event.getType() == ElementType.ALL)
			{
				if (shouldDraw)
				{
					left_height = (!BetaDaysConfig.disableExperienceDrop ? BetaDaysConfig.heartHeight + 8 : BetaDaysConfig.heartHeight);
					right_height = !BetaDaysConfig.disableExperienceDrop ? BetaDaysConfig.heartHeight + 8 : BetaDaysConfig.heartHeight;
					MainWindow res = Minecraft.getInstance().getMainWindow();
					this.renderHealth(event.getMatrixStack(), res.getScaledWidth(), res.getScaledHeight());
					this.renderAir(event.getMatrixStack(), res.getScaledWidth(), res.getScaledHeight());
					this.renderArmor(event.getMatrixStack(), res.getScaledWidth(), res.getScaledHeight());
				}
			}

		}

		if (BetaDaysConfig.oldIngameVersion)
		{
			ElementType type = event.getType();

			if (type == ElementType.ALL && !mc.gameSettings.showDebugInfo)
			{
				renderOldVersion(event.getMatrixStack(), SharedConstants.getVersion().getName());
			}
		}
	}

	@SubscribeEvent
	public void onFOVUpdate(FOVUpdateEvent event)
	{
		try
		{
			if (event.getEntity().isSprinting() && !event.getEntity().isCreative() && BetaDaysConfig.disableSprinting)
			{
				event.setNewfov(1.0F);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void updateHealthTicks()
	{
		++this.updateDelay;

		if (this.updateDelay == 75)
		{
			this.rand.setSeed(updateCounter * 312871);
			++this.updateCounter;
			this.updateDelay = 0;
		}
	}

	private void renderHealth(MatrixStack matrixStack, int width, int height)
	{
		PlayerEntity player = (PlayerEntity) Minecraft.getInstance().getRenderViewEntity();

		if (player != null)
		{
			Minecraft.getInstance().getTextureManager().bindTexture(ICONS);
			matrixStack.push();
			RenderSystem.enableBlend();
			Minecraft.getInstance().getProfiler().startSection("health");
			matrixStack.translate(0.0F, 7.5F, 0.0F);

			int health = MathHelper.ceil(player.getHealth());
			boolean highlight = healthUpdateCounter > (long) updateCounter && (healthUpdateCounter - (long) updateCounter) / 3L % 2L == 1L;

			if (health < this.playerHealth && player.hurtResistantTime > 0)
			{
				this.lastSystemTime = Util.milliTime();
				this.healthUpdateCounter = this.updateCounter + 20;
			}
			else if (health > this.playerHealth && player.hurtResistantTime > 0)
			{
				this.lastSystemTime = Util.milliTime();
				this.healthUpdateCounter = this.updateCounter + 10;
			}

			if (Util.milliTime() - this.lastSystemTime > 1000L)
			{
				this.playerHealth = health;
				this.lastPlayerHealth = health;
				this.lastSystemTime = Util.milliTime();
			}

			this.playerHealth = health;

			int healthLast = this.lastPlayerHealth;

			ModifiableAttributeInstance attrMaxHealth = player.getAttribute(Attributes.MAX_HEALTH);

			float healthMax = (float) attrMaxHealth.getBaseValue();
			float absorb = MathHelper.ceil(player.getAbsorptionAmount());

			int healthRows = MathHelper.ceil((healthMax + absorb) / 2.0F / 10.0F);
			int rowHeight = Math.max(10 - (healthRows - 2), 3);

			this.rand.setSeed(updateCounter * 312871);

			int left = width / 2 - 91;
			int top = height - left_height;

			left_height += (healthRows * rowHeight) - 9;

			if (rowHeight != 10)
				left_height += 10 - rowHeight;

			int regen = -1;

			if (player.isPotionActive(Effects.REGENERATION))
			{
				regen = updateCounter % 25;
			}

			final int TOP = 9 * (Minecraft.getInstance().world.getWorldInfo().isHardcore() ? 5 : 0);
			final int BACKGROUND = (highlight ? 25 : 16);
			int MARGIN = 16;

			if (player.isPotionActive(Effects.POISON))
				MARGIN += 36;
			else if (player.isPotionActive(Effects.WITHER))
				MARGIN += 72;

			float absorbRemaining = absorb;

			for (int i = MathHelper.ceil((healthMax + absorb) / 2.0F) - 1; i >= 0; --i)
			{
				int row = MathHelper.ceil((float) (i + 1) / 10.0F) - 1;
				int x = left + i % 10 * 8;
				int y = top - row * rowHeight;

				if (health <= 4)
					y += rand.nextInt(2);
				if (i == regen)
					y -= 2;

				drawTexturedModalRect(x, y, BACKGROUND, TOP, 9, 9);

				if (highlight)
				{
					if (i * 2 + 1 < healthLast)
						drawTexturedModalRect(x, y, MARGIN + 54, TOP, 9, 9);
					else if (i * 2 + 1 == healthLast)
						drawTexturedModalRect(x, y, MARGIN + 63, TOP, 9, 9);
				}

				if (absorbRemaining > 0.0F)
				{
					if (absorbRemaining == absorb && absorb % 2.0F == 1.0F)
					{
						drawTexturedModalRect(x, y, MARGIN + 153, TOP, 9, 9);
						absorbRemaining -= 1.0F;
					}
					else
					{
						drawTexturedModalRect(x, y, MARGIN + 144, TOP, 9, 9);
						absorbRemaining -= 2.0F;
					}
				}
				else
				{
					if (i * 2 + 1 < health)
						drawTexturedModalRect(x, y, MARGIN + 36, TOP, 9, 9);
					else if (i * 2 + 1 == health)
						drawTexturedModalRect(x, y, MARGIN + 45, TOP, 9, 9);
				}
			}

			Minecraft.getInstance().getProfiler().endSection();
			RenderSystem.disableBlend();
			matrixStack.pop();
		}
	}

	private void renderAir(MatrixStack matrixStack, int width, int height)
	{
		PlayerEntity player = (PlayerEntity) Minecraft.getInstance().getRenderViewEntity();

		if (player != null)
		{
			matrixStack.push();
			Minecraft.getInstance().getProfiler().startSection("air");
			RenderSystem.enableBlend();
			matrixStack.translate(0.75F, 9.0F, 0.0F);

			int left = width / 2 - 83;
			int top = height - left_height - 10;

			if (player.areEyesInFluid(FluidTags.WATER))
			{
				int air = player.getAir();
				int full = MathHelper.ceil((double) (air - 2) * 10.0D / 300.0D);
				int partial = MathHelper.ceil((double) air * 10.0D / 300.0D) - full;
				Minecraft.getInstance().getTextureManager().bindTexture(ICONS);

				for (int i = 0; i < full + partial; ++i)
				{
					drawTexturedModalRect(left + i * 8 - 9, top, (i < full ? 16 : 25), 18, 9, 9);
				}
			}

			Minecraft.getInstance().getProfiler().endSection();
			RenderSystem.disableBlend();
			matrixStack.pop();
		}
	}

	private void renderArmor(MatrixStack matrixStack, int width, int height)
	{
		matrixStack.push();
		Minecraft.getInstance().getProfiler().startSection("armor");
		RenderSystem.enableBlend();
		matrixStack.translate(0.0D, 7.0D, 0.0D);

		int left = width / 2 + 9;
		int top = height - right_height;

		int level = Minecraft.getInstance().player.getTotalArmorValue();

		Minecraft.getInstance().getTextureManager().bindTexture(ICONS);

		for (int i = 1; level > 0 && i < 20; i += 2)
		{
			drawTexturedModalRect(left, top, (i < level) ? 34 : (i == level) ? 25 : 16, 9, 9, 9);
			left += 8;
		}

		right_height += 10;

		Minecraft.getInstance().getProfiler().endSection();
		RenderSystem.disableBlend();
		matrixStack.pop();
	}

	private void renderOldVersion(MatrixStack stack, String version)
	{
		Minecraft mc = Minecraft.getInstance();
		mc.fontRenderer.drawStringWithShadow(stack, "Minecraft Release " + version, 2, 2, -1);
	}

	private void drawTexturedModalRect(int x, int y, int textureX, int textureY, int width, int height)
	{
		Tessellator tessellator = Tessellator.getInstance();
		BufferBuilder vertexBuffer = tessellator.getBuffer();
		vertexBuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
		vertexBuffer.pos(x, y + height, -90).tex((float) textureX * 0.00390625F, (float) (textureY + height) * 0.00390625F).endVertex();
		vertexBuffer.pos(x + width, y + height, -90).tex((float) (textureX + width) * 0.00390625F, (float) (textureY + height) * 0.00390625F).endVertex();
		vertexBuffer.pos(x + width, y, -90).tex((float) (textureX + width) * 0.00390625F, (float) textureY * 0.00390625F).endVertex();
		vertexBuffer.pos(x, y, -90).tex((float) textureX * 0.00390625F, (float) textureY * 0.00390625F).endVertex();
		tessellator.draw();

		// func_225583_a_ tex
	}

}