package com.legacy.betadays.client;

import com.legacy.betadays.BetaDays;
import com.legacy.betadays.client.keybindings.BetaDaysKeyRegistry;

import net.minecraft.client.settings.KeyBinding;
import net.minecraft.client.util.InputMappings;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = BetaDays.MODID, value = { Dist.CLIENT }, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BetaDaysClient
{

	@OnlyIn(Dist.CLIENT)
	public static KeyBinding TOGGLE_FOG_BIND = new KeyBinding("beta.key.toggleFog", InputMappings.INPUT_INVALID.getKeyCode(), "key.categories.misc");

	@OnlyIn(Dist.CLIENT)
	@SubscribeEvent
	public static void clientInit(FMLClientSetupEvent event)
	{
		BetaDays.registerEvent(new BetaClientEvent());
		BetaDays.registerEvent(new BetaOverlayEvent());

		// Subscribe key registry.
		BetaDaysKeyRegistry keyRegistry;
		BetaDays.registerEvent(keyRegistry = new BetaDaysKeyRegistry());

		// Register key binds.
		keyRegistry.register(TOGGLE_FOG_BIND);
	}
}
