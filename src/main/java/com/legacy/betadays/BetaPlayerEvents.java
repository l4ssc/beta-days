package com.legacy.betadays;

import net.minecraft.block.Blocks;
import net.minecraft.block.CakeBlock;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.AxeItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.Food;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDamageEvent;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingExperienceDropEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickBlock;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickItem;
import net.minecraftforge.event.entity.player.PlayerXpEvent;
import net.minecraftforge.event.entity.player.UseHoeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BetaPlayerEvents
{
	public static final AttributeModifier oldSpeed = new AttributeModifier("Beta Speed Modifier", Double.MAX_VALUE, Operation.ADDITION);

	@SubscribeEvent
	public void onPlayerUpdate(LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof PlayerEntity)
		{
			PlayerEntity player = (PlayerEntity) event.getEntityLiving();

			if (BetaDaysConfig.disableCombatCooldown)
			{
				if (!player.getAttribute(Attributes.ATTACK_SPEED).hasModifier(oldSpeed))
					player.getAttribute(Attributes.ATTACK_SPEED).applyNonPersistentModifier(oldSpeed);
			}
			else
			{
				if (player.getAttribute(Attributes.ATTACK_SPEED).hasModifier(oldSpeed))
					player.getAttribute(Attributes.ATTACK_SPEED).removeModifier(oldSpeed);
			}

			if (BetaDaysConfig.hungerDisabled)
			{
				if (player.getFoodStats().getFoodLevel() != 7)
					player.getFoodStats().setFoodLevel(7);
			}

			if (BetaDaysConfig.disableSprinting && !player.isCreative())
			{
				if (player.getAir() < player.getMaxAir() && !player.areEyesInFluid(FluidTags.WATER))
					player.setAir(player.getMaxAir());

				player.setSprinting(false);
			}
		}
	}

	@SubscribeEvent
	public void onPlayerEatCake(RightClickBlock event)
	{
		if (event.getWorld().getBlockState(event.getPos()).getBlock() instanceof CakeBlock)
		{
			if (BetaDaysConfig.hungerDisabled)
			{
				event.getPlayer().heal(2.0F);
			}
		}
	}

	@SubscribeEvent
	public void onPlayerRightClick(RightClickItem event)
	{
		ItemStack item = event.getItemStack();
		PlayerEntity player = (PlayerEntity) event.getEntityLiving();

		if (item.getItem().isFood() && BetaDaysConfig.hungerDisabled)
		{
			Food food = item.getItem().getFood();

			if (player.getHealth() >= player.getMaxHealth())
			{
				event.setCanceled(true);
			}
			else if (BetaDaysConfig.disableFoodStacking)
			{
				item.getItem().onItemUseFinish(item, player.world, player);
				player.heal(food.getHealing());

				event.setCanceled(true);
			}
		}

		if (item.getItem() instanceof BowItem && BetaDaysConfig.originalBow)
		{
			BowItem bow = (BowItem) item.getItem();
			bow.onPlayerStoppedUsing(item, player.world, player, 200);
			event.setCanceled(true);
		}

		if (item.getItem().getClass().getName().equals("mod.torchbowmod.TorchBow") && BetaDaysConfig.originalBow)
		{
			item.onItemUseFinish(player.world, player);
			event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public void onEntityXPDrop(LivingExperienceDropEvent event)
	{
		if (BetaDaysConfig.disableExperienceDrop)
			event.setCanceled(true);
	}

	@SubscribeEvent
	public void onEntityXPPickup(PlayerXpEvent.PickupXp event)
	{
		if (BetaDaysConfig.disableExperienceDrop)
		{
			event.getOrb().remove();
			event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public void onEntityDamaged(LivingDamageEvent event)
	{
		if (BetaDaysConfig.disableCombatCooldown && event.getSource().getImmediateSource() instanceof LivingEntity && ((LivingEntity) event.getSource().getImmediateSource()).getHeldItemMainhand().getItem() instanceof AxeItem)
			event.setAmount(event.getAmount() - 5);
	}

	@SubscribeEvent
	public void onItemFinishUse(LivingEntityUseItemEvent.Finish event)
	{
		if (!BetaDaysConfig.disableFoodStacking && BetaDaysConfig.hungerDisabled && event.getEntityLiving() instanceof PlayerEntity && event.getItem().isFood())
		{
			Food food = event.getItem().getItem().getFood();

			event.getItem().getItem().onItemUseFinish(event.getItem(), event.getEntityLiving().world, event.getEntityLiving());
			event.getEntityLiving().heal(food.getHealing());
		}

		if (event.getItem().getItem() == Items.ROTTEN_FLESH && BetaDaysConfig.hungerDisabled && event.getEntityLiving().world.rand.nextBoolean())
			event.getEntityLiving().addPotionEffect(new EffectInstance(Effects.POISON, 60));
	}

	@SubscribeEvent
	public void onTillDirt(UseHoeEvent event)
	{
		World world = event.getContext().getWorld();

		if ((world.getBlockState(event.getContext().getPos()).getBlock() == Blocks.GRASS_BLOCK || world.getBlockState(event.getContext().getPos()).getBlock() == Blocks.DIRT) && BetaDaysConfig.tillSeeds)
		{
			if (!world.isRemote)
			{
				if (world.rand.nextInt(10) == 0)
				{
					ItemEntity itementity = new ItemEntity(world, event.getContext().getPos().getX(), event.getContext().getPos().getY() + 1, event.getContext().getPos().getZ(), new ItemStack(Items.WHEAT_SEEDS));
					itementity.setDefaultPickupDelay();
					world.addEntity(itementity);
				}
			}
		}
	}
}