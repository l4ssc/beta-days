package com.legacy.betadays;

import org.apache.commons.lang3.tuple.Pair;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;

@Mod.EventBusSubscriber(modid = BetaDays.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BetaDaysConfig
{

	public static final ForgeConfigSpec CLIENT_SPEC;
	public static final ForgeConfigSpec SERVER_SPEC;
	public static final ClientConfig CLIENT;
	public static final ServerConfig SERVER;

	public static boolean disableCombatCooldown;
	public static boolean hungerDisabled;
	public static boolean disableSprinting;
	public static boolean originalBow;
	public static boolean disableExperienceDrop;
	public static boolean disableFoodStacking;
	public static boolean tillSeeds;

	public static int toggleFogMaxDistance;
	public static boolean oldIngameVersion;
	public static boolean oldHud;
	public static boolean disableNetherFog;
	public static boolean customDimensionMessages;
	public static boolean enableClassicMenu;
	public static boolean disableCombatSounds;
	public static int heartHeight;
	public static boolean disableCombatSweep;

	static
	{
		{
			final Pair<ClientConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
		{
			final Pair<ServerConfig, ForgeConfigSpec> pair = new ForgeConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = pair.getLeft();
			SERVER_SPEC = pair.getRight();
		}
	}

	private static String translate(String key)
	{
		return BetaDays.MODID + ".config." + key + ".name";
	}

	@SubscribeEvent
	public static void onLoadConfig(final ModConfig.ModConfigEvent event)
	{
		ModConfig config = event.getConfig();

		if (config.getSpec() == CLIENT_SPEC)
		{
			ConfigBakery.bakeClient(config);
		}
		else if (config.getSpec() == SERVER_SPEC)
		{
			ConfigBakery.bakeServer(config);
		}
	}

	private static class ClientConfig
	{
		public final ForgeConfigSpec.ConfigValue<Boolean> oldIngameVersion;
		public final ForgeConfigSpec.ConfigValue<Boolean> oldHud;
		public final ForgeConfigSpec.ConfigValue<Integer> toggleFogMaxDistance;

		public final ForgeConfigSpec.ConfigValue<Boolean> disableNetherFog;
		public final ForgeConfigSpec.ConfigValue<Boolean> customDimensionMessages;

		public final ForgeConfigSpec.ConfigValue<Boolean> enableClassicMenu;
		public final ForgeConfigSpec.ConfigValue<Boolean> disableCombatSounds;

		public final ForgeConfigSpec.ConfigValue<Integer> heartHeight;

		public ClientConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Client side changes.").push("client");
			oldIngameVersion = builder.translation(translate("oldIngameVersion")).comment("Displays Minecraft version at the top of screen when in game.").define("oldIngameVersion", false);
			oldHud = builder.translation(translate("oldHud")).comment("Gives the classic HUD, removing the hunger bar, and adjusting the position of armor bar.").define("oldHud", false);
			toggleFogMaxDistance = builder.translation(translate("toggleFogMaxDistance")).comment("The maximum distance fog can be toggled to.").define("toggleFogMaxDistance", 16);

			disableNetherFog = builder.translation(translate("disableNetherFog")).comment("Removes environmental fog from the Nether Dimension.").define("disableNetherFog", false);
			customDimensionMessages = builder.translation(translate("customDimensionMessages")).comment("Enable custom dimension entry messages. (Vanilla Dimensions)").define("customDimensionMessages", false);
			enableClassicMenu = builder.translation(translate("enableClassicMenu")).comment("Enable the classic menu. (May cause mod compatibility issues when used with other menu mods)").define("enableClassicMenu", false);
			disableCombatSounds = builder.translation(translate("disableCombatSounds")).comment("Disable the 1.9+ combat sounds.").define("disableCombatSounds", false);

			heartHeight = builder.translation(translate("heartHeight")).comment("The height offset of the hearts when using the old HUD.").define("heartHeight", 32);

			builder.pop();
		}
	}

	private static class ServerConfig
	{
		public final ForgeConfigSpec.ConfigValue<Boolean> disableCombatCooldown;
		public final ForgeConfigSpec.ConfigValue<Boolean> hungerDisabled;
		public final ForgeConfigSpec.ConfigValue<Boolean> disableSprinting;
		public final ForgeConfigSpec.ConfigValue<Boolean> originalBow;
		public final ForgeConfigSpec.ConfigValue<Boolean> disableExperienceDrop;
		public final ForgeConfigSpec.ConfigValue<Boolean> disableFoodStacking;
		public final ForgeConfigSpec.ConfigValue<Boolean> tillSeeds;
		public final ForgeConfigSpec.ConfigValue<Boolean> disableCombatSweep;

		public ServerConfig(ForgeConfigSpec.Builder builder)
		{
			builder.comment("Server and Client side changes.").push("common");
			disableCombatCooldown = builder.translation(translate("disableCombatCooldown")).comment("Disables the 1.9+ combat cooldown.").define("disableCombatCooldown", false);
			hungerDisabled = builder.translation(translate("hungerDisabled")).comment("Disables hunger, food gives health instead.").define("hungerDisabled", false);
			disableSprinting = builder.translation(translate("disableSprinting")).comment("Disables sprinting.").define("disableSprinting", false);
			originalBow = builder.translation(translate("originalBow")).comment("Allows for instantly shooting bows.").define("originalBow", false);
			disableExperienceDrop = builder.translation(translate("disableExperienceDrop")).comment("Disables mobs dropping experience. (Disables the experience bar along with it)").define("disableExperienceDrop", false);
			disableFoodStacking = builder.translation(translate("disableFoodStacking")).comment("Disables the ability to stack food items. When enabled, this also allows you to instantly eat food.").define("disableFoodStacking", false);
			tillSeeds = builder.translation(translate("tillSeeds")).comment("Allows for seeds to drop randomly when tilling dirt.").define("tillSeeds", false);
			disableCombatSweep = builder.translation(translate("disableCombatSweep")).comment("Disables the 1.9+ sweep attack. Only applies when the Sweeping Edge enchantment is not used.").define("disableCombatSweep", false);

			builder.pop();
		}
	}

	@SuppressWarnings("unused")
	private static class ConfigBakery
	{
		private static ModConfig clientConfig;
		private static ModConfig serverConfig;

		public static void bakeClient(ModConfig config)
		{
			clientConfig = config;
			oldIngameVersion = CLIENT.oldIngameVersion.get();
			oldHud = CLIENT.oldHud.get();
			toggleFogMaxDistance = CLIENT.toggleFogMaxDistance.get();

			disableNetherFog = CLIENT.disableNetherFog.get();
			customDimensionMessages = CLIENT.customDimensionMessages.get();
			enableClassicMenu = CLIENT.enableClassicMenu.get();
			disableCombatSounds = CLIENT.disableCombatSounds.get();

			heartHeight = CLIENT.heartHeight.get();
		}

		public static void bakeServer(ModConfig config)
		{
			serverConfig = config;
			disableCombatCooldown = SERVER.disableCombatCooldown.get();
			hungerDisabled = SERVER.hungerDisabled.get();
			disableSprinting = SERVER.disableSprinting.get();
			originalBow = SERVER.originalBow.get();
			disableExperienceDrop = SERVER.disableExperienceDrop.get();
			disableFoodStacking = SERVER.disableFoodStacking.get();
			tillSeeds = SERVER.tillSeeds.get();
			disableCombatSweep = SERVER.disableCombatSweep.get();
		}
	}
}
